# rpg-charecters-main
RPG Characters

An assignment as part of a course at Experis Academy, by Noroff. A short description of what is required and objectives: What you are required to do is model and demonstrate a simple RPG character system. No game logic is required aside from the aspects explicitly mentioned in req document. This Task is designed to test modelling and class design skills.

Usage
Open the project in your preferred IDE: Intellij. This is the best and recommended option as you will find it easier to debug and add/remvove functionality. 
